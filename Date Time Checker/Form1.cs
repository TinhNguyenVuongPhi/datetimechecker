﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Date_Time_Checker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MinimizeBox = false;
            MaximizeBox = false;
        }
        public int DayinMonth(int year, byte month)
        {
            if (month == 1
               || month == 3
               || month == 5
               || month == 7
               || month == 8
               || month == 10
               || month == 12)
                return 31;
            else if (month == 4
                       || month == 6
                       || month == 9
                       || month == 11)
                return 30;
            else if (month==2)
            {
                if (year % 400 == 0)
                    return 29;
                else if (year % 100 == 0)
                    return 28;
                else if (year % 4 == 0)
                    return 29;
                else return 28;
            }
            return 0;
        }
        public bool IsValidDay(int year, byte month, byte day)
        {
            if (month <= 12 && month >= 1)
            {
                if (day >= 1)
                {
                    if (day <= DayinMonth(year, month))
                        return true;
                    else return false;

                }
                else return false;
            }
            else return false;
        }
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult obj = MessageBox.Show("Are you sure to exit?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (obj == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            txtday.Text = "";
            txtmonth.Text = "";
            txtyear.Text = "";

        }

        private void txtday_TextChanged(object sender, EventArgs e)
        {

        }
        public bool CheckIsNumber(string val)
        {
            foreach(char obj in val)
            {
                if (!char.IsDigit(obj))
                    return false;
            }
            return true;
        }
        public bool CheckRangeDay(byte day)
        {
            if (day < 1 || day > 31)
                return false;
            else
                return true;
        }
        public bool CheckRangeMonth(byte m)
        {
            if (m < 1 || m > 12)
                return false;
            else
                return true;

                //MessageBox.Show("Input data for Month is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
            
        public bool CheckRangeYear(int y)
        {
            if (y < 1000 || y > 3000)
                return false;
            else
                return true;
                    //MessageBox.Show("Input data for Year is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void btnCheck_Click(object sender, EventArgs e)
        {
            string text1 = txtday.Text;
            string text2 = txtmonth.Text;
            string text3 = txtyear.Text;
            try
            {
                if (text1 != "" && text2 != "" && text3 != "")
                {
                    if (CheckIsNumber(txtday.Text) == false
                          || CheckIsNumber(txtmonth.Text) == false
                          || CheckIsNumber(txtyear.Text) == false)
                    {
                        if (CheckIsNumber(txtday.Text) == false)
                            MessageBox.Show("Input data for Day is incorrect format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else if (CheckIsNumber(txtmonth.Text) == false)
                            MessageBox.Show("Input data for Month is incorrect format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                            MessageBox.Show("Input data for Year is incorrect format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    if (CheckRangeDay(Convert.ToByte(txtday.Text)) &&
                        CheckRangeMonth(Convert.ToByte(txtmonth.Text)) &&
                        CheckRangeYear(Convert.ToInt16(txtyear.Text)))
                    {
                        if (IsValidDay(Convert.ToInt16(txtyear.Text),
                                      Convert.ToByte(txtmonth.Text),
                                      Convert.ToByte(txtday.Text)) == true)
                            MessageBox.Show(Convert.ToByte(txtday.Text) + "/" + Convert.ToByte(txtmonth.Text) + "/" + Convert.ToInt32(txtyear.Text) + " is a correct date time!");
                        else
                            MessageBox.Show(Convert.ToByte(txtday.Text) + "/" + Convert.ToByte(txtmonth.Text) + "/" + Convert.ToInt32(txtyear.Text) + " is NOT a correct date time!");
                    }
                    else if (CheckRangeDay(Convert.ToByte(txtday.Text)) == false ||
                             CheckRangeDay(Convert.ToByte(txtmonth.Text)) == false ||
                             CheckRangeDay(Convert.ToByte(txtyear.Text)) == false)
                    {
                        if (CheckRangeDay(Convert.ToByte(txtday.Text)) == false)
                           MessageBox.Show("Input data for Day is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else if  (CheckRangeDay(Convert.ToByte(txtmonth.Text)) == false)
                            MessageBox.Show("Input data for Month is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else if (CheckRangeDay(Convert.ToByte(txtyear.Text)) == false)
                            MessageBox.Show("Input data for Year is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Input data is incorrect format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch
            {
                MessageBox.Show("Input data is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
