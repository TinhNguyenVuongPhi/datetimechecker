﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Date_Time_Checker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestDateTimeChecker
{
    [TestClass]
    public class UnitTest1
    {
        public object ExceptionAssert { get; private set; }

        // Function DayInMonth
        [TestMethod()]
        public void DayInMonthTest_UTCID01()
        {
            Form1 f = new Form1();
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(31, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID02()
        {
            Form1 f = new Form1();
            byte m = 3;
            int y = 2018;
            Assert.AreEqual(31, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID03()
        {
            Form1 f = new Form1();
            byte m = 5;
            int y = 2018;
            Assert.AreEqual(31, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID04()
        {
            Form1 f = new Form1();
            byte m = 7;
            int y = 2018;
            Assert.AreEqual(31, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID05()
        {
            Form1 f = new Form1();
            byte m = 8;
            int y = 2018;
            Assert.AreEqual(31, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID06()
        {
            Form1 f = new Form1();
            byte m = 10;
            int y = 2018;
            Assert.AreEqual(31, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID07()
        {
            Form1 f = new Form1();
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(31, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID08()
        {
            Form1 f = new Form1();
            byte m = 4;
            int y = 2018;
            Assert.AreEqual(30, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID09()
        {
            Form1 f = new Form1();
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(30, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID011()
        {
            Form1 f = new Form1();
            byte m = 9;
            int y = 2018;
            Assert.AreEqual(30, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID012()
        {
            Form1 f = new Form1();
            byte m = 11;
            int y = 2018;
            Assert.AreEqual(30, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID013()
        {
            Form1 f = new Form1();
            byte m = 2;
            int y = 2000;
            Assert.AreEqual(29, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID014()
        {
            Form1 f = new Form1();
            byte m = 2;
            int y = 2004;
            Assert.AreEqual(29, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID015()
        {
            Form1 f = new Form1();
            byte m = 2;
            int y = 2017;
            Assert.AreEqual(28, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID016()
        {
            Form1 f = new Form1();
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(0, f.DayinMonth(y, m));
        }
        [TestMethod()]
        public void DayInMonthTest_UTCID017()
        {
            Form1 f = new Form1();
            byte m = 0;
            int y = 2018;
            Assert.AreEqual(0, f.DayinMonth(y, m));
        }
        //Function IsValidday
        [TestMethod()]
        public void IsValidDayTest_UTCID01()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID02()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID03()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID04()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 2;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID05()
        //{
        //    Form1 f = new Form1();
        //    byte d = 15;
        //    byte m = -1;
        //    int y = 2018;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID06()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID07()
        //{
        //    Form1 f = new Form1();
        //    byte d = 15;
        //    byte m = 256;
        //    int y = 2018;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID08()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 6;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID09()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 1;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID010()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 12;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID011()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 2;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID012()
        //{
        //    Form1 f = new Form1();
        //    byte d = 15;
        //    byte m = -1;
        //    int y = 2016;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID013()
        {
            Form1 f = new Form1();
            byte d = 15;
            byte m = 13;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID014()
        //{
        //    Form1 f = new Form1();
        //    byte d = 15;
        //    byte m = 256;
        //    int y = 2016;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID015()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID016()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID017()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID018()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 2;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID019()
        //{
        //    Form1 f = new Form1();
        //    byte d = 1;
        //    byte m = -1;
        //    int y = 2018;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID020()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID021()
        //{
        //    Form1 f = new Form1();
        //    byte d = 1;
        //    byte m = 256;
        //    int y = 2018;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID022()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 6;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID023()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 1;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID024()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 12;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID025()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 2;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID026()
        //{
        //    Form1 f = new Form1();
        //    byte d = 1;
        //    byte m = -1;
        //    int y = 2016;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID027()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 13;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID028()
        //{
        //    Form1 f = new Form1();
        //    byte d = 1;
        //    byte m = 256;
        //    int y = 2016;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID029()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID030()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID031()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID032()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 2;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID033()
        //{
        //    Form1 f = new Form1();
        //    byte d = 28;
        //    byte m = -1;
        //    int y = 2018;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID034()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID035()
        //{
        //    Form1 f = new Form1();
        //    byte d = 28;
        //    byte m = 256;
        //    int y = 2018;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID036()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 6;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID037()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 1;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID038()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 12;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID039()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 2;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID040()
        //{
        //    Form1 f = new Form1();
        //    byte d = 28;
        //    byte m = -1;
        //    int y = 2016;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID041()
        {
            Form1 f = new Form1();
            byte d = 28;
            byte m = 13;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID042()
        //{
        //    Form1 f = new Form1();
        //    byte d = 28;
        //    byte m = 256;
        //    int y = 2016;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}        [TestMethod()]
        public void IsValidDayTest_UTCID43()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID44()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID045()
        {
            Form1 f = new Form1();
            byte d = 1;
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID046()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 2;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID047()
        //{
        //    Form1 f = new Form1();
        //    byte d = 31;
        //    byte m = -1;
        //    int y = 2018;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID048()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID049()
        //{
        //    Form1 f = new Form1();
        //    byte d = 31;
        //    byte m = 256;
        //    int y = 2018;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID050()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 6;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID051()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 1;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID052()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 12;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID053()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 2;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID054()
        //{
        //    Form1 f = new Form1();
        //    byte d = 31;
        //    byte m = -1;
        //    int y = 2016;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID055()
        {
            Form1 f = new Form1();
            byte d = 31;
            byte m = 13;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID056()
        //{
        //    Form1 f = new Form1();
        //    byte d = 31;
        //    byte m = 256;
        //    int y = 2016;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}        [TestMethod()]
        public void IsValidDayTest_UTCID57()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID58()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID59()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID60()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 2;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID61()
        //{
        //    Form1 f = new Form1();
        //    byte d = 32;
        //    byte m = -1;
        //    int y = 2018;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID62()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID63()
        //{
        //    Form1 f = new Form1();
        //    byte d = 32;
        //    byte m = 256;
        //    int y = 2018;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID64()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 6;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID065()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 1;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID066()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 12;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID067()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 2;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID068()
        //{
        //    Form1 f = new Form1();
        //    byte d = 32;
        //    byte m = -1;
        //    int y = 2016;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID069()
        {
            Form1 f = new Form1();
            byte d = 32;
            byte m = 13;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID070()
        //{
        //    Form1 f = new Form1();
        //    byte d = 32;
        //    byte m = 256;
        //    int y = 2016;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}        [TestMethod()]
        public void IsValidDayTest_UTCID071()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID073()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID074()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID075()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 2;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID076()
        //{
        //    Form1 f = new Form1();
        //    byte d = 30;
        //    byte m = -1;
        //    int y = 2018;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID077()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID078()
        //{
        //    Form1 f = new Form1();
        //    byte d = 30;
        //    byte m = 256;
        //    int y = 2018;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID079()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 6;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID080()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 1;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID081()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 12;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID082()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 2;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID083()
        //{
        //    Form1 f = new Form1();
        //    byte d = 30;
        //    byte m = -1;
        //    int y = 2016;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID084()
        {
            Form1 f = new Form1();
            byte d = 30;
            byte m = 13;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID085()
        //{
        //    Form1 f = new Form1();
        //    byte d = 30;
        //    byte m = 256;
        //    int y = 2016;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}        [TestMethod()]
        public void IsValidDayTest_UTCID086()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 6;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID087()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 1;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID088()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 12;
            int y = 2018;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID089()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 2;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID090()
        //{
        //    Form1 f = new Form1();
        //    byte d = 29;
        //    byte m = -1;
        //    int y = 2018;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID091()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 13;
            int y = 2018;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID092()
        //{
        //    Form1 f = new Form1();
        //    byte d = 29;
        //    byte m = 256;
        //    int y = 2018;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID93()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 6;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID094()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 1;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID095()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 12;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        [TestMethod()]
        public void IsValidDayTest_UTCID096()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 2;
            int y = 2016;
            Assert.AreEqual(true, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID097()
        //{
        //    Form1 f = new Form1();
        //    byte d = 29;
        //    byte m = -1;
        //    int y = 2016;
        //    Assert.AreEqual(0, f.IsValidDay(y, m, d));
        //}
        [TestMethod()]
        public void IsValidDayTest_UTCID098()
        {
            Form1 f = new Form1();
            byte d = 29;
            byte m = 13;
            int y = 2016;
            Assert.AreEqual(false, f.IsValidDay(y, m, d));
        }
        //[TestMethod()]
        //public void IsValidDayTest_UTCID099()
        //{
        //    Form1 f = new Form1();
        //    byte d = 29;
        //    byte m = 256;
        //    int y = 2016;
        //    Assert.AreEqual(false, f.IsValidDay(y, m, d));
        //}
        // Function CheckRankMonth
        [TestMethod()]
        public void CheckRangeDayTest()
        {
            Form1 f = new Form1();
            byte d = 1;
            Assert.AreEqual(true, f.CheckRangeMonth(d));
        }
        [TestMethod()]
        public void CheckRangeMonthTest_UTCID01()
        {
            Form1 f = new Form1();
            byte m = 5;
            Assert.AreEqual(true, f.CheckRangeMonth(m));
        }

        [TestMethod()]
        public void CheckRangeMonthTest_UTCID02()
        {
            Form1 f = new Form1();
            byte m = 12;
            Assert.AreEqual(true, f.CheckRangeMonth(m));
        }

        [TestMethod()]
        public void CheckRangeMonthTest_UTCID03()
        {
            Form1 f = new Form1();
            byte m = 1;
            Assert.AreEqual(true, f.CheckRangeMonth(m));
        }

        [TestMethod()]
        public void CheckRangeMonthTest_UTCID04()
        {
            //byte chay tu 1
            Form1 f = new Form1();
            byte m = 0;
            Assert.AreEqual(false, f.CheckRangeMonth(m));
        }

        [TestMethod()]
        public void CheckRangeMonthTest_UTCID05()
        {
            Form1 f = new Form1();
            byte m = 13;
            Assert.AreEqual(false, f.CheckRangeMonth(m));
        }

        [TestMethod()]
        public void CheckRangeMonthTest_UTCID06()
        {
            Form1 f = new Form1();
            byte m = 255;
            Assert.AreEqual(false, f.CheckRangeMonth(m));
        }

        [TestMethod()]
        public void CheckRangeYearTest_UTCID01()
        {
            Form1 f = new Form1();
            int y = 2004;
            Assert.AreEqual(true, f.CheckRangeYear(y));
        }

        [TestMethod()]
        public void CheckRangeYearTest_UTCID02()
        {
            Form1 f = new Form1();
            int y = 1000;
            Assert.AreEqual(true, f.CheckRangeYear(y));
        }

        [TestMethod()]
        public void CheckRangeYearTest_UTCID03()
        {
            Form1 f = new Form1();
            int y = 3000;
            Assert.AreEqual(true, f.CheckRangeYear(y));
        }

        [TestMethod()]
        public void CheckRangeYearTest_UTCID04()
        {
            Form1 f = new Form1();
            int y = 999;
            Assert.AreEqual(false, f.CheckRangeYear(y));
        }

        [TestMethod()]
        public void CheckRangeYearTest_UTCID05()
        {
            Form1 f = new Form1();
            int y = 3001;
            Assert.AreEqual(false, f.CheckRangeYear(y));
        }

        [TestMethod()]
        public void CheckRangeYearTest_UTCID06()
        {
            Form1 f = new Form1();
            int y = 65536;
            Assert.AreEqual(false, f.CheckRangeYear(y));
        }
        Form1 testtree = new Form1();

        [TestMethod]
        public void CheckIsNunberTest_UTCID01()
        {
            Assert.IsTrue(testtree.CheckIsNumber("1"));
        }
        [TestMethod]
        public void CheckIsNunberTest_UTCID02()
        {
            Assert.IsFalse(testtree.CheckIsNumber("a"));
        }
        [TestMethod]
        public void CheckIsNunberTest_UTCID03()
        {
            Assert.IsFalse(testtree.CheckIsNumber("@"));
        }
        [TestMethod]
        public void CheckIsNunberTest_UTCID04()
        {
            Assert.IsFalse(testtree.CheckIsNumber("a1"));
        }
        [TestMethod]
        public void CheckIsNunberTest_UTCID05()
        {
            Assert.IsFalse(testtree.CheckIsNumber(" "));
        }


        [TestMethod]
        public void CheckRangeDayTest_UTCID01()
        {
            Assert.IsTrue(testtree.CheckRangeDay(15));
        }
        [TestMethod]
        public void CheckRangeDayTests_UTCID02()
        {
            Assert.IsTrue(testtree.CheckRangeDay(1));
        }
        [TestMethod]
        public void CheckRangeDayTest_UTCID03()
        {
            Assert.IsTrue(testtree.CheckRangeDay(31));
        }
        [TestMethod]
        public void CheckRangeDayTest_UTCID04()
        {
            Assert.IsFalse(testtree.CheckRangeDay(32));
        }
        [TestMethod]
        public void CheckRangeDayTest_UTCID05()
        {
            Assert.IsFalse(testtree.CheckRangeDay(0));
        }
        [TestMethod]
        public void CheckRangeDayTest_UTCID06()
        {
            Assert.IsFalse(testtree.CheckRangeDay(255));
        }
    }
}
